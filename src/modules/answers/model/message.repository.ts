import {
    Injectable,
    Logger,
    NotFoundException,
    InternalServerErrorException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { throwError, from } from 'rxjs';
import { mergeMap, map, catchError, tap } from 'rxjs/operators';

import { neverNullable } from '@utils/rxjs.util';
import { IMessage, MMessage } from './message.model';
import { FindMessagesDto } from '../dto/find-messages.dto';
import { CreateMessageDto } from '../dto/create-message.dto';

const logger = new Logger('MessageRepository');

@Injectable()
export class MessageRepository {
    constructor(
        @InjectModel('Message') private readonly messageModel: Model<MMessage>,
    ) {}

    public async find({
        limit = 30,
        skip = 0,
        userId,
    }: FindMessagesDto): Promise<IMessage[]> {
        return await from(
            this.messageModel
                .find({ user: userId })
                .sort({ createdAt: -1 })
                .skip(skip)
                .limit(limit)
                .exec(),
        )
            .pipe(
                mergeMap(neverNullable),
                map((a: any) => a.map(b => b.toObject())),
                catchError(e =>
                    throwError(new NotFoundException('Messages not found')),
                ),
            )
            .toPromise();
    }

    public async create(newMessage: IMessage) {
        return await from(this.messageModel.create(newMessage))
            .pipe(
                mergeMap(neverNullable),
                catchError(e =>
                    throwError(
                        new InternalServerErrorException(
                            'Creating New Message Error',
                        ),
                    ),
                ),
            )
            .toPromise();
    }
}
