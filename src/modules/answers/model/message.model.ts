import { Schema, Document } from 'mongoose';

export const MessageSchema = new Schema(
    {
        text: {
            type: String,
            required: true,
            trim: true,
        },
        content: {
            type: String,
            trim: true,
        },
        from: {
            type: String,
            required: true,
            enum: ['BOT', 'USER'],
        },
        user: {
            type: Schema.Types.ObjectId,
            ref: 'User',
            required: true,
        },
    },
    {
        timestamps: true,
    },
);

export interface IMessage {
    text?: string;
    content?: string;
    user?: any;
    from: string;

    createdAt?: Date;
    updatedAt?: Date;
}

export interface MMessage extends Document, IMessage {}
