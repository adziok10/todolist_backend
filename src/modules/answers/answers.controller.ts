// import {
//     Controller,
//     Get,
//     Query,
//     UseGuards,
//     Post,
//     UseInterceptors,
//     Req,
//     Body,
// } from '@nestjs/common';

// import { AnswersService } from './answers.service';
// import { AuthInterceptor } from '../auth/guards/auth.guard';
// import { CreateMessageDto } from './dto/create-message.dto';
// import { FindMessagesDto } from './dto/find-messages.dto';

// @Controller('/answers')
// export class AnswersController {
//     constructor(private readonly answersService: AnswersService) {}

//     @Post()
//     @UseInterceptors(AuthInterceptor)
//     public getHello(@Body() msg: CreateMessageDto, @Req() { user }: any) {
//         return this.answersService.askElsticSearch(msg, user);
//     }

//     @Get()
//     @UseInterceptors(AuthInterceptor)
//     public loadMessages(@Query() args: FindMessagesDto, @Req() { user }: any) {
//         return this.answersService.loadMessages(args, user);
//     }
// }
