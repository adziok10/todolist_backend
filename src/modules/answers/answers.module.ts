// import { Module } from '@nestjs/common';
// import { MongooseModule } from '@nestjs/mongoose';
// import { ElasticsearchModule } from '@nestjs/elasticsearch';

// import { AnswersController } from './answers.controller';
// import { AnswersService } from './answers.service';
// import { SharedModule } from '../../shared/shared.module';
// import { ConfigService } from '@modules/config/config.service';
// import { ConfigModule } from '@modules/config/config.module';
// import { ESService } from './es.service';
// import { MessageSchema } from './model/message.model';
// import { MessageRepository } from './model/message.repository';

// @Module({
//     imports: [
//         SharedModule,
//         ElasticsearchModule.registerAsync({
//             imports: [ConfigModule],
//             useFactory: async (configService: ConfigService) => ({
//                 node: configService.elasticSearchUrl,
//             }),
//             inject: [ConfigService],
//         }),
//         MongooseModule.forFeature([{ name: 'Message', schema: MessageSchema }]),
//     ],
//     controllers: [AnswersController],
//     providers: [AnswersService, ESService, MessageRepository],
// })
// export class AnswersModule {}
