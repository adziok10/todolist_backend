import {
    IsNotEmpty,
    IsString,
    IsNumber,
    IsMongoId,
    IsIn,
    IsOptional,
} from 'class-validator';

export class CreateMessageDto {
    @IsString()
    @IsNotEmpty()
    text?: string;

    // @IsIn(['BOT', 'USER'])
    // @IsNotEmpty()
    // from: 'BOT' | 'USER';

    @IsString()
    @IsOptional()
    content: string;

    // @IsMongoId()
    // @IsNotEmpty()
    // user: string;
}
