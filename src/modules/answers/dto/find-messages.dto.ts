import {
    IsNotEmpty,
    IsEmail,
    IsString,
    IsNumber,
    IsMongoId,
    IsOptional,
} from 'class-validator';

export class FindMessagesDto {
    @IsNumber()
    @IsOptional()
    skip?: number = 0;

    @IsNumber()
    @IsOptional()
    limit?: number = 30;

    @IsMongoId()
    @IsOptional()
    userId?: string;
}
