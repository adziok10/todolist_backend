// import { Injectable } from '@nestjs/common';

// import { ESService } from './es.service';
// import { MessageRepository } from './model/message.repository';
// import { CreateMessageDto } from './dto/create-message.dto';
// import { IMessage } from './model/message.model';
// import { IUser } from '@modules/user/model/user.model';

// import { answers } from './answers.constant';
// import { FindMessagesDto } from './dto/find-messages.dto';

// @Injectable()
// export class AnswersService {
//     constructor(
//         private esService: ESService,
//         private messageRepository: MessageRepository,
//     ) {}

//     public async loadMessages(args: FindMessagesDto, user) {
//         return await this.messageRepository.find({ ...args, userId: user._id });
//     }

//     public async askElsticSearch(messageData: CreateMessageDto, user: IUser) {
//         const newMessage: IMessage = await this.messageRepository.create({
//             ...messageData,
//             user: user._id,
//             from: 'USER',
//         });

//         return await this.generateResponse(newMessage);
//     }

//     public async generateResponse({ text, user }: IMessage): Promise<IMessage> {
//         const { title, content } = answers[this.randomInt(0, answers.length)];

//         return await this.messageRepository.create({
//             text: title,
//             content,
//             user,
//             from: 'BOT',
//         });
//     }

//     private randomInt(from: number, to: number): number {
//         return Math.floor(Math.random() * to + from);
//     }
// }
