// import { Injectable, HttpService } from '@nestjs/common';
// import { map, catchError } from 'rxjs/operators';

// import { ConfigService } from '@modules/config/config.service';
// import { ElasticsearchService } from '@nestjs/elasticsearch';

// @Injectable()
// export class ESService {

//     constructor(private readonly elasticsearchService: ElasticsearchService, private config: ConfigService) { }

//     // public async multiSearch(text: string): Promise<any[]> {
//     //     try {
//     //         const { body: { hits } } = await this.elasticsearchService.search({
//     //             index: this.config.elasticSearchWorkspace,
//     //             size: 3,
//     //             body: {
//     //                 query: {
//     //                     multi_match: {
//     //                         query: text,
//     //                         fields: ['content^2', 'title', 'searchTitle'],
//     //                     },
//     //                 },
//     //             },
//     //             _source: ['title'],
//     //             pretty: true,
//     //         });

//     //         return hits && hits.hits || [];

//     //     } catch (e) {
//     //         return [];
//     //     }
//     // }

//     public async getHighlights({ text, _id }: { text: string, _id?: string }) {
//         try {
//             const { body: { hits } } = await this.elasticsearchService.search({
//                 index: this.config.elasticSearchWorkspace,
//                 size: 3,
//                 body: {
//                     query: {
//                         match: {
//                             content: text,
//                         },
//                     },
//                     highlight: {
//                         number_of_fragments: 1,
//                         fields: {
//                             content: {
//                                 fragment_size: 50,
//                                 pre_tags: '',
//                                 post_tags: '',
//                             },
//                         },
//                         encoder: 'default',
//                     },
//                 },
//                 _source: ['value', 'title'],
//                 pretty: true,
//             });

//             return hits && hits.hits || [];

//         } catch (e) {
//             return [];
//         }
//     }

// }
