import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { UserController } from './user.controller';
import { UserService } from './user.service';
import { UserSchema } from './model/user.model';
import { UserRoleSchema } from './model/user-role.model';
import { UserRepository } from './model/user.repository';

@Module({
    imports: [
        MongooseModule.forFeature([{
            name: 'User',
            schema: UserSchema,
        }, {
            name: 'UserRole',
            schema: UserRoleSchema,
        }]),
    ],
    controllers: [UserController],
    providers: [UserService, UserRepository],
    exports: [UserService],
})
export class UserModule {}
