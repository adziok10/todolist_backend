import { Schema, Document } from 'mongoose';

export const UserRoleSchema = new Schema({
    name: {
        type: String,
        required: true,
    },
    key: {
        type: String,
        required: true,
    },
    permissions: [{
        name: {
            type: String,
            required: true,
        },
        key: {
            type: String,
            required: true,
        },
        level: {
            type: String,
            required: true,
            enum: ['admin', 'default', 'none'],
        },
    }],
}, {
    timestamps: true,
});

export interface IUserRole {
    _id: any;
    name: string;
    key: string;
    permissions: [{
        name: string,
        key: string,
        level: 'admin' | 'default' | 'none',
    }];

    createdAt: Date;
    updatedAt: Date;
}

export interface DUserRole extends Document, IUserRole {}
