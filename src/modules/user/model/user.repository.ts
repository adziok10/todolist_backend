import { Injectable, Logger, NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Schema } from 'mongoose';
import { throwError, Observable, from } from 'rxjs';
import { mergeMap, map, catchError, tap } from 'rxjs/operators';

import { neverNullable } from '@utils/rxjs.util';
import { IUser, MUser } from './user.model';
import { FindUserByIdDto } from '../dto/find-user-by-id.dto';
import { FindUserDto } from '../dto/find-user.dto';
import { NewUserDto } from '../dto/new-user.dto';

const logger = new Logger('UserRepository');

@Injectable()
export class UserRepository {
    constructor(@InjectModel('User') private readonly userModel: Model<MUser>) { }

    public async findOneById({ _id }: FindUserByIdDto): Promise<IUser> {
        return await from(this.userModel.findOne({ _id }).exec())
            .pipe(
                mergeMap(neverNullable),
                map((a: any) => a.toObject()),
                map(this.removePassword),
                catchError(e => throwError(
                    new NotFoundException('User not found'),
                )),
            ).toPromise();
    }

    public async findOne({ email }: FindUserDto): Promise<IUser> {
        return await from(this.userModel.findOne({ email }).exec())
            .pipe(
                mergeMap(neverNullable),
                map((a: any) => a.toObject()),
                catchError(e => throwError(
                    new NotFoundException('User not found'),
                )),
            )
            .toPromise();
    }

    public async create(newUser: NewUserDto) {
        return await from(this.userModel.create(newUser))
            .pipe(
                mergeMap(neverNullable),
                map((a: any) => a.toObject()),
                map(this.removePassword),
                catchError(e =>
                    throwError(
                        new InternalServerErrorException(
                            'Creating new user error',
                        ),
                    ),
                ),
            )
            .toPromise();
    }

    private removePassword(data) {
        const { password, ...rest } = data;
        return rest;
    }
}
