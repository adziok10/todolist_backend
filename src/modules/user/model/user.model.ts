import { Schema, Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export const UserSchema = new Schema({
    firstname: {
        type: String,
        minlength: 1,
        trim: true,
    },
    lastname: {
        type: String,
        minlength: 1,
        trim: true,
    },
    login: {
        type: String,
        minlength: 5,
        trim: true,
    },
    email: {
        type: String,
        required: true,
        minlength: 6,
    },
    password: {
        type: String,
        required: true,
        minlength: 8,
    },
    // role: {
    //     type: Schema.Types.ObjectId,
    //     ref: 'UserRole',
    // },
}, {
    timestamps: true,
});

export class IUser {
    @ApiProperty()
    // tslint:disable-next-line: variable-name
    _id: any;

    @ApiProperty()
    firstname?: string;

    @ApiProperty()
    lastname?: string;

    @ApiProperty()
    login?: string;

    @ApiProperty()
    email: string;

    password: string;

    // @ApiProperty()
    // role?: any;

    @ApiProperty()
    createdAt: Date;

    @ApiProperty()
    updatedAt: Date;
}

export interface MUser extends Document, IUser {}
