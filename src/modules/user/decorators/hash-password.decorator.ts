import { Observable } from 'rxjs';
import * as argon from 'argon2';

import { IUser } from '../model/user.model';

export const HashPassword = () => {
    return (
        target: any,
        propertyKey: string | symbol,
        descriptor: TypedPropertyDescriptor<(...params: any[]) => IUser | Promise<IUser> | Observable<IUser>>,
    ) => {
        descriptor = descriptor || Object.getOwnPropertyDescriptor(target, propertyKey);

        const originalMethod = descriptor.value;

        descriptor.value = async function() {
            const { password } = arguments[0];
            // hashing password when aragon2 start working
            const hashedPassword = await argon.hash(password);

            return await originalMethod.apply(this, [{ ...arguments[0], password: hashedPassword }]);
        };
    };
};
