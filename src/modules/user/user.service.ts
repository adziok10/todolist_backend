import { Injectable } from '@nestjs/common';

import { UserRepository } from './model/user.repository';
import { FindUserByIdDto } from './dto/find-user-by-id.dto';
import { FindUserDto } from './dto/find-user.dto';
import { IUser } from './model/user.model';
import { NewUserDto } from './dto/new-user.dto';
import { HashPassword } from './decorators/hash-password.decorator';

@Injectable()
export class UserService {

    constructor(private userRepository: UserRepository) {}

    public findOneById({ _id }: FindUserByIdDto) {
        return this.userRepository.findOneById({ _id });
    }

    public async findOne({ email }: FindUserDto): Promise<IUser> {
        return await this.userRepository.findOne({ email });
    }

    @HashPassword()
    public async create(newUser: NewUserDto) {
        console.log(newUser)
        return this.userRepository.create(newUser);
    }
}
