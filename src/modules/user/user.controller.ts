import { Controller, Post, Body } from '@nestjs/common';
import { ApiResponse } from '@nestjs/swagger';
import { OmitProperties } from 'ts-essentials';

import { UserService } from './user.service';
import { NewUserDto } from './dto/new-user.dto';
import { IUser } from './model/user.model';

@Controller('/user')
export class UserController {
    constructor(private readonly userService: UserService) {}

    // @Get()
    // // @UseGuards(AuthGuard)
    // getHello(@Query('text') text: string) {
    //     return this.userService.test();
    // }

    @Post()
    @ApiResponse({ status: 201, description: 'The user has been successfully created.' })
    @ApiResponse({ status: 400, description: 'Wrong data.'})
    public newUser(@Body() newUser: NewUserDto): OmitProperties<IUser, 'password'> {
        return this.userService.create(newUser) as any;
    }
}
