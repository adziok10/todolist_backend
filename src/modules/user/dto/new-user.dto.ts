import { IsNotEmpty, IsEmail, IsString } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class NewUserDto {

    @ApiProperty()
    @IsEmail()
    @IsNotEmpty()
    email: string;

    @ApiProperty({
        minLength: 5,
    })
    @IsString()
    @IsNotEmpty()
    login: string;

    @ApiProperty({
        minLength: 8,
    })
    @IsString()
    @IsNotEmpty()
    password: string;
}
