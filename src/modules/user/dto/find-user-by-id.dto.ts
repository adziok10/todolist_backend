import { IsMongoId, IsNotEmpty } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FindUserByIdDto {

    @ApiProperty()
    @IsNotEmpty()
    @IsMongoId()
    // tslint:disable-next-line: variable-name
    _id: string;

}
