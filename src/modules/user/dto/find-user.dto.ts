import { IsMongoId, IsNotEmpty, IsEmail } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FindUserDto {

    @ApiProperty()
    @IsEmail()
    @IsNotEmpty()
    email: string;

}
