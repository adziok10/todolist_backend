import { IsNumber, IsOptional, IsArray } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class FindUsersDto {

    @ApiProperty()
    @IsNumber()
    limit?: number = 30;

    @ApiProperty()
    @IsNumber()
    skip?: number = 0;

    @ApiProperty()
    @IsOptional()
    @IsArray()
    role?: string[];

}
