import { Controller, Get, Post, Body, Param, Put, Delete, UseInterceptors } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { TodoService } from './todo.service';
import { ITodo } from './model/todo.model';
import { NewTodoDto } from './dto/new-todo.dto';
import { GetTodosQuery } from './dto/get-todos.query';
import { GetTodoQuery } from './dto/get-todo.query';
import { AuthInterceptor } from '@modules/auth/guards/auth.interceptor';

@ApiTags('TODO')
@Controller('/todo')
@UseInterceptors(AuthInterceptor)
export class TodoController {
    constructor(private readonly todoService: TodoService) {}

    @ApiResponse({ status: 201, description: 'The todo has been successfully created.', type: ITodo })
    @ApiResponse({ status: 400, description: 'Wrong data.'})
    @Post()
    public newTodo(@Body() data: NewTodoDto): Promise<ITodo> {
        return this.todoService.create(data);
    }

    @ApiResponse({ status: 200, description: 'Todo with passed _id.', type: ITodo })
    @ApiResponse({ status: 404, description: 'Todo not found.'})
    @Get(':_id')
    public getTodo(@Param() parmas: GetTodoQuery): Promise<ITodo> {
        return this.todoService.findOne(parmas);
    }

    @ApiResponse({ status: 200, description: 'The todo has been successfully updated.', type: ITodo })
    @ApiResponse({ status: 400, description: 'Wrong data.'})
    @ApiResponse({ status: 404, description: 'Todo not found.'})
    @Put(':_id')
    public updateTodo(@Param() parmas: GetTodoQuery, @Body() data: NewTodoDto): Promise<ITodo> {
        return this.todoService.updateOne(parmas, data);
    }

    @ApiResponse({ status: 200, description: 'The todo has been successfully deleted.' })
    @ApiResponse({ status: 400, description: 'Wrong data.'})
    @ApiResponse({ status: 404, description: 'Todo not found.'})
    @Delete(':_id')
    public deleteTodo(@Param() parmas: GetTodoQuery) {
        return this.todoService.deleteOne(parmas);
    }

}
