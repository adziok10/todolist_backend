import { IsNotEmpty, IsMongoId, IsOptional, IsNumber, Max, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';

export class GetTodosQuery {

    // @ApiProperty()
    @IsMongoId()
    @IsOptional()
    owner?: string | Types.ObjectId;

    @ApiProperty()
    @IsMongoId()
    @IsNotEmpty()
    todoList?: string | Types.ObjectId;

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    skip?: number = 0;

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    @Max(100)
    @Min(1)
    limit?: number = 10;
}
