import { IsNotEmpty, IsString, IsHexColor, IsOptional, IsMongoId, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';

export class NewTodoDto {

    @ApiProperty({
        description: 'Todo list title',
    })
    @IsString()
    @IsNotEmpty()
    @MinLength(4)
    title: string;

    @ApiProperty({
        description: 'Todo list ObjectId',
    })
    @IsMongoId()
    @IsNotEmpty()
    todoList: string | Types.ObjectId;

}
