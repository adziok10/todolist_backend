import { IsNotEmpty, IsMongoId } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetTodoQuery {

    @ApiProperty({
        description: 'Todo ObjectId',
    })
    @IsMongoId()
    @IsNotEmpty()
    // tslint:disable-next-line: variable-name
    _id: string;

}
