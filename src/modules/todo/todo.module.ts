import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { TodoController } from './todo.controller';
import { TodoService } from './todo.service';
import { TodoSchema } from './model/todo.model';
import { TodoRepository } from './model/todo.repository';

@Module({
    imports: [
        MongooseModule.forFeature([{
            name: 'Todo',
            schema: TodoSchema,
        }]),
    ],
    controllers: [TodoController],
    providers: [TodoService, TodoRepository],
    exports: [TodoService],
})
export class TodoModule {}
