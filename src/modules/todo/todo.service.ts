import { Injectable } from '@nestjs/common';

import { TodoRepository } from './model/todo.repository';
import { ITodo, MTodo } from './model/todo.model';
import { NewTodoDto } from './dto/new-todo.dto';
import { GetTodoQuery } from './dto/get-todo.query';
import { TodoCache } from './decorators/todo-cache.decorator';
import { TodoCacheService } from 'src/shared/services/todo-cache.service';

@Injectable()
export class TodoService {
    constructor(
        private readonly todoRepository: TodoRepository,
        private readonly todoCacheService: TodoCacheService,
    ) {}

    // @TodoCache(['INSERT', 'LIST_EXISTS'])
    public async create(data: NewTodoDto): Promise<ITodo> {
        return await this.todoRepository.create(data);
    }

    public findByTodoListId(searchParams: string) {
        return this.todoRepository.find(searchParams);
    }

    @TodoCache(['EXISTS'])
    public async findOne(searchParams: GetTodoQuery) {
        return this.todoRepository.findOne(searchParams);
    }

    @TodoCache(['EXISTS', 'LIST_EXISTS'])
    public async updateOne(searchParams: GetTodoQuery, body: Partial<MTodo>) {
        return this.todoRepository.updateOneById(searchParams, body);
    }

    @TodoCache(['EXISTS', 'REMOVE'])
    public async deleteOne(searchParams: GetTodoQuery) {
        return this.todoRepository.deleteOneById(searchParams);
    }

}
