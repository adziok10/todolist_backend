import { Schema, Document, Types } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';

export const TodoSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    todoList: {
        type: Schema.Types.ObjectId,
        ref: 'TodoList',
        required: true,
    },
    status: {
        type: String,
        enum: ['DONE', 'IN_PROGRESS', 'NONE'],
        required: true,
        default: 'NONE',
    },
}, {
    timestamps: true,
});

export class ITodo {
    @ApiProperty()
    // tslint:disable-next-line: variable-name
    _id: string | any;

    @ApiProperty()
    title: string;

    @ApiProperty()
    todoList: string | Types.ObjectId;

    @ApiProperty()
    status: 'DONE' | 'IN_PROGRESS' | 'NONE';

    @ApiProperty()
    createdAt: Date;

    @ApiProperty()
    updatedAt: Date;
}

export interface MTodo extends Document, ITodo {}
