import { Injectable, Logger, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { from, throwError } from 'rxjs';
import { DeepPartial } from 'ts-essentials/dist/types';
import { Model } from 'mongoose';

import { MTodo, ITodo } from './todo.model';
import { NewTodoDto } from '../dto/new-todo.dto';
import { mergeMap, map, catchError } from 'rxjs/operators';
import { neverNullable, convertToObject, convertToObjectMap } from '@utils/rxjs.util';
import { GetTodoQuery } from '../dto/get-todo.query';

const logger = new Logger('TodoRepository');

@Injectable()
export class TodoRepository {
    constructor(@InjectModel('Todo') private readonly todoModel: Model<MTodo>) { }

    public async create(newTodo: NewTodoDto): Promise<ITodo> {
        return await from(this.todoModel.create(newTodo))
            .pipe(
                mergeMap(neverNullable),
                map(convertToObject),
                catchError(e => throwError(
                    new InternalServerErrorException('Creating new todo list error',e),
                )),
            )
            .toPromise();
    }

    public async findOne({ _id }: GetTodoQuery): Promise<MTodo> {
        return await from(this.todoModel.findById(_id).exec())
            .pipe(
                mergeMap(neverNullable),
                map(convertToObject),
                catchError(e => throwError(
                    new NotFoundException('Todo not found'),
                )),
            )
            .toPromise();
    }

    public find(todoList: string): any {
        return from(this.todoModel.find({ todoList }).exec())
            .pipe(
                mergeMap(neverNullable),
                map(convertToObjectMap),
                catchError(e => throwError(
                    new NotFoundException('Todo not found'),
                )),
            );
    }

    public async updateOneById({ _id }: GetTodoQuery, body: Partial<MTodo>) {
        return await from(this.todoModel.findByIdAndUpdate(_id, body, { new: true }).exec())
            .pipe(
                mergeMap(neverNullable),
                map(convertToObject),
                catchError(e => throwError(
                    new NotFoundException('Todo not found'),
                )),
            )
            .toPromise();
    }

    public async deleteOneById({ _id }: GetTodoQuery) {
        return await from(this.todoModel.findByIdAndDelete(_id).exec())
            .pipe(
                mergeMap(neverNullable),
                map(convertToObject),
                catchError(e => throwError(
                    new NotFoundException('Todo not found'),
                )),
            )
            .toPromise();
    }

}
