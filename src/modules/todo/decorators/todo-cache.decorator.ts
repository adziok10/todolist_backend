import { Observable } from 'rxjs';
import { BadRequestException } from '@nestjs/common';

type ActionTypes = 'EXISTS' | 'REMOVE' | 'INSERT' | 'LIST_EXISTS';

// tslint:disable: no-unused-expression
export const TodoCache = (types: ActionTypes[] = []) => {
    return (
        target: any,
        propertyKey: string | symbol,
        descriptor: TypedPropertyDescriptor<(...params: any[]) => any | Promise<any> | Observable<any>>,
    ) => {
        descriptor = descriptor || Object.getOwnPropertyDescriptor(target, propertyKey);
        const originalMethod = descriptor.value;

        descriptor.value = async function() {
            const { _id, todoList } = arguments[0] || {};
            const { todoList: tdList } = arguments && arguments[1] || {};

            types.includes('LIST_EXISTS') && todoList && await !this.todoCacheService.isExists(todoList) === false && e(1);

            types.includes('LIST_EXISTS') && tdList && await this.todoCacheService.isExists(tdList) === false && e(1);

            types.includes('EXISTS') && _id && await this.todoCacheService.isExists(_id) === false && e(2);

            const result =  await originalMethod.apply(this, arguments);

            types.includes('REMOVE') && result && !(result instanceof Error) && (
                result._id && this.todoCacheService.removeValueFromUserCache(result._id)
                    ||
                _id && this.todoCacheService.removeValueFromUserCache(_id)
            );

            types.includes('INSERT') && result && result._id && await this.todoCacheService.pushValueToUserCache(result._id);

            return result;
        };
    };
};

function e(type: number) {
    const msg = type === 1 && 'Invalid todo list id' || 'Invalid todo id';

    throw new BadRequestException(msg);
}
