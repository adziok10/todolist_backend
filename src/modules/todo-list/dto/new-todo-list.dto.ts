import { IsNotEmpty, IsString, IsHexColor, IsOptional, IsMongoId, MinLength } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';
import { Types } from 'mongoose';

export class NewTodoListDto {

    @ApiProperty({
        description: 'Todo list title',
    })
    @IsString()
    @IsNotEmpty()
    @MinLength(4)
    title: string;

    @ApiProperty({
        default: '#A2F7B5',
        required: false,
        description: 'Todo list theme color',
    })
    @IsHexColor()
    @IsOptional()
    theme?: string;

    // @ApiProperty({
    //     description: 'Owner ObjectId',
    // })
    @IsMongoId()
    @IsOptional()
    owner?: string | Types.ObjectId;

}
