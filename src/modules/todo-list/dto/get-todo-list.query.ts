import { IsNotEmpty, IsMongoId, IsOptional } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetTodoListQuery {

    @ApiProperty({
        description: 'TodoList ObjectId',
    })
    @IsMongoId()
    @IsNotEmpty()
    // tslint:disable-next-line: variable-name
    _id: string;

    @IsMongoId()
    @IsOptional()
    owner?: string;

}
