import { IsNotEmpty, IsMongoId, IsOptional, IsNumber, Max, Min } from 'class-validator';
import { ApiProperty } from '@nestjs/swagger';

export class GetTodoListsQuery {

    // @ApiProperty()
    @IsMongoId()
    @IsOptional()
    owner?: string;

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    skip?: number = 0;

    @ApiProperty()
    @IsNumber()
    @IsNotEmpty()
    @Max(100)
    @Min(1)
    limit?: number = 10;
}
