import { Controller, Get, Post, Body, UseInterceptors, Param, Put, Delete } from '@nestjs/common';
import { ApiResponse, ApiTags } from '@nestjs/swagger';

import { TodoListService } from './todo-list.service';
import { NewTodoListDto } from './dto/new-todo-list.dto';
import { AuthInterceptor } from '../auth/guards/auth.interceptor';
import { GetTodoListQuery } from './dto/get-todo-list.query';
import { ITodoList } from './model/todo-list.model';
import { GetTodoListsQuery } from './dto/get-todo-lists.query';

@ApiTags('TODO LIST')
@UseInterceptors(AuthInterceptor)
@Controller('/todolist')
export class TodoListController {
    constructor(private readonly todolistService: TodoListService) {}

    @ApiResponse({ status: 201, description: 'The todo list has been successfully created.' })
    @ApiResponse({ status: 400, description: 'Wrong data.'})
    @Post()
    public newTodoList(@Body() data: NewTodoListDto): Promise<ITodoList> {
        return this.todolistService.create(data);
    }

    @ApiResponse({ status: 200, description: 'Todo lists list.' })
    @ApiResponse({ status: 404, description: 'TodoLists not found.'})
    @Get()
    public getTodoLists(@Body() data: GetTodoListsQuery): Promise<ITodoList[]> {
        return this.todolistService.find(data);
    }

    @ApiResponse({ status: 200, description: 'Todo list with passed _id.' })
    @ApiResponse({ status: 404, description: 'TodoList not found.'})
    @Get(':_id')
    public getTodoList(@Param() parmas: GetTodoListQuery): Promise<ITodoList> {
        return this.todolistService.findOne(parmas);
    }

    @ApiResponse({ status: 200, description: 'The todo list has been successfully updated.' })
    @ApiResponse({ status: 400, description: 'Wrong data.'})
    @ApiResponse({ status: 404, description: 'TodoList not found.'})
    @Put(':_id')
    public updateTodoList(@Param() parmas: GetTodoListQuery, @Body() data: NewTodoListDto): Promise<ITodoList> {
        return this.todolistService.updateOne(parmas, data);
    }

    @ApiResponse({ status: 200, description: 'The todo list has been successfully deleted.' })
    @ApiResponse({ status: 400, description: 'Wrong data.'})
    @ApiResponse({ status: 404, description: 'TodoList not found.'})
    @Delete(':_id')
    public deleteTodoList(@Param() parmas: GetTodoListQuery) {
        return this.todolistService.deleteOne(parmas);
    }
}
