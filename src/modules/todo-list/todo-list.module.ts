import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { TodoListController } from './todo-list.controller';
import { TodoListService } from './todo-list.service';
import { TodoListSchema } from './model/todo-list.model';
import { TodoListRepository } from './model/todo-list.repository';
import { TodoModule } from '@modules/todo/todo.module';

@Module({
    imports: [
        MongooseModule.forFeature([{
            name: 'TodoList',
            schema: TodoListSchema,
        }]),
        TodoModule,
    ],
    controllers: [TodoListController],
    providers: [TodoListService, TodoListRepository],
    exports: [TodoListService],
})
export class TodoListModule {}
