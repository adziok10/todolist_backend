import { Injectable, Logger, InternalServerErrorException, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { from, throwError, merge, zip, combineLatest, forkJoin, isObservable, Observable, of } from 'rxjs';
import { mergeMap, map, catchError, tap, mergeMapTo } from 'rxjs/operators';
import { Model } from 'mongoose';
import { MarkRequired, DeepPartial } from 'ts-essentials';

import { neverNullable, convertToObject, convertToObjectMap } from '@utils/rxjs.util';
import { MTodoList, ITodoList } from './todo-list.model';
import { NewTodoListDto } from '../dto/new-todo-list.dto';
import { GetTodoListQuery } from '../dto/get-todo-list.query';
import { GetTodoListsQuery } from '../dto/get-todo-lists.query';
import { MTodo } from '@modules/todo/model/todo.model';
import { TodoService } from '@modules/todo/todo.service';

const logger = new Logger('TodoListRepository');

@Injectable()
export class TodoListRepository {
    constructor(
        @InjectModel('TodoList') private readonly todoListModel: Model<MTodoList>,
        private readonly todoService: TodoService,
    ) { }

    public async create(newTodoList: MarkRequired<NewTodoListDto, 'owner'>): Promise<ITodoList> {
        return await from(this.todoListModel.create(newTodoList))
            .pipe(
                mergeMap(neverNullable),
                map(convertToObject),
                catchError(e =>
                    throwError(
                        new InternalServerErrorException(
                            'Creating new todo list error',
                        ),
                    ),
                ),
            )
            .toPromise();
    }

    public async find({ owner, skip, limit }: MarkRequired<GetTodoListsQuery, 'owner'> ): Promise<MTodoList[]> {
        return await from(this.todoListModel.find({ owner }).skip(skip).limit(limit).exec())
            .pipe(
                mergeMap(neverNullable),
                map(convertToObjectMap),
                catchError(e => throwError(
                    new NotFoundException('Todo list not found'),
                )),
            )
            .toPromise();
    }

    public async findOne(query: MarkRequired<GetTodoListQuery, 'owner'> ): Promise<any> {
        return await from(this.todoListModel.findOne(query).exec())
            .pipe(
                mergeMap(neverNullable),
                map(convertToObject),
                mergeMap(todoList => forkJoin({
                        todoList: of(todoList),
                        todos: this.todoService.findByTodoListId(todoList._id),
                    }),
                ),
                map(({ todoList, todos }: any) => ({ ...todoList, todos })),
                catchError(e => throwError(
                    new NotFoundException('Todo list not found'),
                )),
            )
            .toPromise();
    }

    public async updateOneById({ _id, owner }, body: DeepPartial<MTodoList>) {
        return await from(this.todoListModel.findOneAndUpdate({ _id, owner }, body, { new: true }).exec())
            .pipe(
                mergeMap(neverNullable),
                map((a: any) => a.toObject()),
                catchError(e => throwError(
                    new NotFoundException('Todo list not found'),
                )),
            )
            .toPromise();
    }

    public async deleteOneById({ _id, owner }) {
        return await from(this.todoListModel.findOneAndDelete({ _id, owner }).exec())
            .pipe(
                mergeMap(neverNullable),
                map((a: any) => a.toObject()),
                catchError(e => throwError(
                    new NotFoundException('Todo list not found'),
                )),
            )
            .toPromise();
    }

}
