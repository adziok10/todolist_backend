import { Schema, Document } from 'mongoose';
import { ApiProperty } from '@nestjs/swagger';
// tslint:disable: variable-name

export const TodoListSchema = new Schema({
    title: {
        type: String,
        required: true,
        trim: true,
        minlength: 1,
    },
    theme: {
        type: String,
        default: '#A2F7B5',
    },
    owner: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
}, {
    timestamps: true,
});

export class ITodoList {
    @ApiProperty()
    _id: string | any; // tslint:disable: variable-name

    @ApiProperty()
    title: string;

    @ApiProperty()
    theme: string;

    @ApiProperty()
    owner: string | any;

    @ApiProperty()
    createdAt: Date;

    @ApiProperty()
    updatedAt: Date;
}

export interface MTodoList extends Document, ITodoList {}
