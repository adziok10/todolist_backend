import { Injectable, UseInterceptors } from '@nestjs/common';
import { MarkRequired } from 'ts-essentials';
import { Types } from 'mongoose';

import { TodoListRepository } from './model/todo-list.repository';
import { NewTodoListDto } from './dto/new-todo-list.dto';
import { UserStoreService } from 'src/shared/services/user-store.service';
import { GetTodoListQuery } from './dto/get-todo-list.query';
import { ITodoList } from './model/todo-list.model';
import { GetTodoListsQuery } from './dto/get-todo-lists.query';
import { TodoCache } from '@modules/todo/decorators/todo-cache.decorator';
import { TodoCacheService } from 'src/shared/services/todo-cache.service';
import { AuthInterceptor } from '@modules/auth/guards/auth.interceptor';

@Injectable()
@UseInterceptors(AuthInterceptor)
export class TodoListService {
    constructor(
        private readonly todoListRepository: TodoListRepository,
        private readonly userStore: UserStoreService,
        private readonly todoCacheService: TodoCacheService,
    ) {}

    @TodoCache(['INSERT'])
    public async create(data: NewTodoListDto): Promise<ITodoList> {
        const dataWithOwner: MarkRequired<NewTodoListDto, 'owner'> = { ...data, owner: Types.ObjectId(this.userStore.user._id) };

        return await this.todoListRepository.create(dataWithOwner);
    }

    public async find(data: GetTodoListsQuery): Promise<ITodoList[]> {
        const query: MarkRequired<GetTodoListsQuery, 'owner'> = { ...data, owner: this.userStore.user._id };

        return this.todoListRepository.find(query);
    }

    public async findOne(data: GetTodoListQuery) {
        const queryWithOwner: MarkRequired<GetTodoListQuery, 'owner'> = { ...data, owner: this.userStore.user._id };

        return this.todoListRepository.findOne(queryWithOwner);
    }

    public async updateOne(searchParams: GetTodoListQuery, body: NewTodoListDto) {
        const query: MarkRequired<GetTodoListQuery, 'owner'> = { ...searchParams, owner: this.userStore.user._id };

        return this.todoListRepository.updateOneById(query, body);
    }

    @TodoCache(['REMOVE'])
    public async deleteOne(data: GetTodoListQuery) {
        const query: MarkRequired<GetTodoListQuery, 'owner'> = { ...data, owner: this.userStore.user._id };

        return this.todoListRepository.deleteOneById(query);
    }

}
