import { Injectable, Scope, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as randToken from 'rand-token';
import * as argon from 'argon2';

import { UserService } from '@modules/user/user.service';
import { RefreshTokenRepository } from './model/refresh-token.repository';
import { IUser } from '@modules/user/model/user.model';

// tslint:disable: variable-name
@Injectable()
export class AuthService {
    constructor(
        private readonly usersService: UserService,
        private readonly jwtService: JwtService,
        private readonly rtRepository: RefreshTokenRepository,
    ) {}

    public async validateUser(email: string, pass: string): Promise<any> {
        const user = await this.usersService.findOne({ email });

        if (user && await argon.verify(user.password, pass)) {
            const { password, ...result } = user;

            const access_token = this.jwtService.sign({
                ...result,
                tokenCreatedAt: Date.now(),
            });
            const refresh_token = user._id + randToken.uid(256);

            await this.rtRepository.create({
                refreshToken: refresh_token,
                userId: user._id,
            });

            return {
                access_token,
                refresh_token,
            };
        }

        throw new UnauthorizedException('Bad password');
    }

    public async refreshToken(a: string) {
        const { userId: _id }: any = await this.rtRepository.findOne({
            refreshToken: a,
        });

        const user = await this.usersService.findOneById({ _id });

        const { password, ...result } = user;
        const access_token = this.jwtService.sign({
            ...result,
            tokenCreatedAt: Date.now(),
        });

        return { access_token, refresh_token: a };
    }

    public async findUserById(_id: string): Promise<IUser> {
        const { password, ...rest } = await this.usersService.findOneById({
            _id,
        });

        return { ...rest } as IUser;
    }
}
