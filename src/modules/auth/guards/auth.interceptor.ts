import {
    Injectable,
    Logger,
    UnauthorizedException,
    ExecutionContext,
    NestInterceptor,
    CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';
import { JwtService } from '@nestjs/jwt';

import { AuthService } from '@modules/auth/auth.service';
import { IUser } from '@modules/user/model/user.model';
import { UserStoreService } from 'src/shared/services/user-store.service';

const logger = new Logger('AuthGuard');

@Injectable()
export class AuthInterceptor implements NestInterceptor {
    constructor(
        private readonly authService: AuthService,
        private readonly jwtService: JwtService,
        private readonly userStore: UserStoreService,
    ) {}

    async intercept(
        context: ExecutionContext,
        next: CallHandler<any>,
    ): Promise<Observable<any>> {
        const request = context.switchToHttp().getRequest();
        const { authorization } = request.headers;

        request.user = await this.checkUser(authorization);
        this.userStore.user = request.user;

        return next.handle();
    }

    private async checkUser(bearerToken: string): Promise<IUser> {
        try {
            const token = bearerToken.replace('Bearer ', '');
            const { _id }: any = this.jwtService.decode(token);

            return await this.authService.findUserById(_id);
        } catch (error) {
            throw new UnauthorizedException('Token error');
        }
    }

    // @GetHeaderFromExecutionContext()
    // canActivate(
    //     headers: any,
    // ): boolean | Promise<boolean> | Observable<boolean> {
    //     const { authorization } = headers;

    //     return this.authorizeClientFromContext(context) && next.handle().pipe(tap(() => true)) || new AuthenticationError('Inavlid token')

    //     return authorization && this.isValidToken(authorization);
    // }

    // private async authorizeClientFromContext(context): Promise<boolean> {
    //     const token = this.getTokenFromContext(context)

    //     const client: IClientAuthorized = await this.jwtService.decode(token) as IClientAuthorized

    //     if (!!client) {
    //         await this.clientStore.loadClient(client)
    //     }

    //     return !!client
    // }

    // private getTokenFromContext(context): string {
    //     const { request } = GqlExecutionContext.create(context).getContext()

    //     return this.excludeBearerTokenFromHeaders(request)
    // }

    // private excludeBearerTokenFromHeaders(request): string {
    //     const authorization = request.get('Authorization')

    //     return authorization && ((authorization.includes('Bearer ') && authorization.replace('Bearer ', ''))
    //         || (authorization.includes('Wandlee ') && authorization.replace('Wandlee ', ''))) || new AuthenticationError('Missing token')
    // }
}
