import { Controller, UseGuards, Post, Request, Req, Body } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';

import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
    constructor(private readonly authService: AuthService) {}

    @Post('login')
    public login(@Body() a) {
        console.log(a)
        return this.authService.validateUser(a.email, a.password);
    }

    @Post('token')
    public token(@Body() a) {
        return this.authService.refreshToken(a.refresh_token);
    }
}
