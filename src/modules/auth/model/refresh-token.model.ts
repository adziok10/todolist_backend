import { Schema, Document } from 'mongoose';

export const RefreshTokenSchema = new Schema({
    refreshToken: {
        type: String,
        minlength: 1,
        required: true,
        trim: true,
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
}, {
    timestamps: true,
});

export interface IRefreshToken {
    _id: any;
    refreshToken: string;
    userId: any;

    createdAt: Date;
    updatedAt: Date;
}

export interface MRefreshToken extends Document, IRefreshToken {}
