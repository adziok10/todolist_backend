import { Injectable, Logger, NotFoundException, InternalServerErrorException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { throwError, from } from 'rxjs';
import { mergeMap, map, catchError, tap } from 'rxjs/operators';

import { neverNullable } from '@utils/rxjs.util';
import { MRefreshToken } from './refresh-token.model';

const logger = new Logger('UserRepository');

@Injectable()
export class RefreshTokenRepository {
    constructor(@InjectModel('RefreshToken') private readonly refreshTokenModel: Model<MRefreshToken>) { }

    public async findOne({ refreshToken }) {
        return await from(this.refreshTokenModel.findOne({ refreshToken }).exec())
            .pipe(
                mergeMap(neverNullable),
                map(this.returnRefreshToken),
                catchError(e => throwError(
                    new NotFoundException('Invalid refresh token'),
                )),
            ).toPromise();
    }

    public async create({ refreshToken, userId }) {
        return await from(this.refreshTokenModel.create({ refreshToken, userId }))
            .pipe(
                mergeMap(neverNullable),
                map(this.returnRefreshToken),
                catchError(e => throwError(
                    new InternalServerErrorException('Creating Reresh Token Error'),
                )),
            ).toPromise();
    }

    private returnRefreshToken(obj) {
        const { refreshToken, userId } = obj.toObject();
        return { refresh_token: refreshToken, userId };
    }
}
