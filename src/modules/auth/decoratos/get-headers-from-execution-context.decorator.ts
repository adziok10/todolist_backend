import { Observable } from 'rxjs';

/**
 * Function gets headers from Execution Context
 * Pass array of strings, with headers what you want to get
 *
 * @param {string[]} [allowedHeaders=['Authorization']] array of required headers
 * @returns
 */
export const GetHeaderFromExecutionContext = (allowedHeaders: string[] = ['authorization']) => {
    return (
        target: any,
        propertyKey: string | symbol,
        descriptor: TypedPropertyDescriptor<(...params: any[]) => boolean | Promise<boolean> | Observable<boolean>>,
    ) => {
        descriptor = descriptor || Object.getOwnPropertyDescriptor(target, propertyKey);

        const originalMethod = descriptor.value;

        descriptor.value = async function() {
            const { headers } = arguments[0].switchToHttp().getRequest();
            const arg = Object.entries(headers)
                            .filter(([key, v]) => allowedHeaders.includes(key))
                            .reduce((prev, [key, val]) => ({ ...prev, [key]: val }), {});

            return await originalMethod.apply(this, [arg]);
        };
    };
};
