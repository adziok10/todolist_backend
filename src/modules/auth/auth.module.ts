import { Module, HttpModule, Global } from '@nestjs/common';
import { JwtModule, JwtService } from '@nestjs/jwt';
import { MongooseModule } from '@nestjs/mongoose';

import { AuthService } from './auth.service';
import { AuthInterceptor } from './guards/auth.interceptor';
import { UserModule } from '@modules/user/user.module';
import { AuthController } from './auth.controller';
import { ConfigService } from '@modules/config/config.service';
import { ConfigModule } from '@modules/config/config.module';
import { RefreshTokenSchema } from './model/refresh-token.model';
import { RefreshTokenRepository } from './model/refresh-token.repository';

@Global()
@Module({
    imports: [
        // PassportModule,
        JwtModule.registerAsync({
            inject: [ConfigService],
            imports: [ConfigModule],
            useFactory: (configSerivce: ConfigService) => ({
                secret: configSerivce.jwtSecret,
                signOptions: { expiresIn: '360h' },
            }),
        }),
        HttpModule,
        UserModule,
        MongooseModule.forFeature([
            { name: 'RefreshToken', schema: RefreshTokenSchema },
        ]),
    ],
    controllers: [AuthController],
    providers: [AuthService, AuthInterceptor, RefreshTokenRepository],
    exports: [AuthService, AuthInterceptor, JwtModule],
})
export class AuthModule {}
