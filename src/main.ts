import { NestFactory } from '@nestjs/core';
import { ValidationPipe } from '@nestjs/common';
// import * as passport from 'passport';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';

import { CoreModule } from './core/core.module';
import { ConfigService } from '@modules/config/config.service';
import { CorsInterceptor } from './shared/interceptors/cors.interceptor';

async function bootstrap() {
    const app = await NestFactory.create(CoreModule);

    app.useGlobalInterceptors(new CorsInterceptor());

    const config: ConfigService = app.get(ConfigService);

    app.useGlobalPipes(new ValidationPipe());

    app.setGlobalPrefix('api');

    app.enableCors({
        methods: '*',
        origin: '*',
    });

    const options = new DocumentBuilder()
        .addBearerAuth()
        .setTitle('TODO API')
        .setDescription('TODO API description')
        .setVersion('1.0')
        .addTag('todo')
        .build();

    const document = SwaggerModule.createDocument(app, options);

    SwaggerModule.setup('api', app, document);

    await app.listen(config.port);
}

bootstrap();
