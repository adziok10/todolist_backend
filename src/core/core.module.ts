import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { RedisModule } from 'nestjs-redis';

// import { AnswersModule } from '@modules/answers/answers.module';
import { ConfigModule } from '@modules/config/config.module';
import { ConfigService } from '@modules/config/config.service';
import { UserModule } from '@modules/user/user.module';
import { CoreController } from './core.controller';
import { CoreService } from './core.service';
import { AuthModule } from '@modules/auth/auth.module';
import { TodoListModule } from '@modules/todo-list/todo-list.module';
import { TodoModule } from '@modules/todo/todo.module';
import { SharedModule } from 'src/shared/shared.module';

@Module({
    imports: [
        ConfigModule,
        MongooseModule.forRootAsync({
            imports: [ConfigModule],
            useFactory: async (configService: ConfigService) => ({
                uri: configService.connectionString,
                useNewUrlParser: true,
                useUnifiedTopology: true,
                useFindAndModify: false,
            }),
            inject: [ConfigService],
        }),
        RedisModule.forRootAsync({
            useFactory: (configService: ConfigService) => ({ url: configService.redis }),
            //useFactory: async (configService: ConfigService) => configService.get('redis'),
            inject: [ConfigService],
            imports: [ConfigModule],
        }),
        SharedModule,
        AuthModule,
        // AnswersModule,
        UserModule,
        TodoListModule,
        TodoModule,
    ],
    controllers: [CoreController],
    providers: [CoreService],
})
export class CoreModule { }
