import { Module, HttpModule, Global } from '@nestjs/common';

import { UserStoreService } from './services/user-store.service';
import { RedisClientService } from './services/redis-client.service';
import { TodoCacheService } from './services/todo-cache.service';

@Global()
@Module({
    imports: [HttpModule],
    providers: [UserStoreService, RedisClientService, TodoCacheService],
    exports: [UserStoreService, RedisClientService, TodoCacheService],
})
export class SharedModule { }
