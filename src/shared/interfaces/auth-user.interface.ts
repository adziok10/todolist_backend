export interface IAuthUser {
    _id: string;
    email: string;
    login: string;
}
