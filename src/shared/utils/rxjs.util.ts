import { throwError, of } from 'rxjs';

const isNullable = (data: any) => data === null || data === undefined;

export const convertToObject = (a: any) => a.toObject();

export const convertToObjectMap = a => a.map(convertToObject);

export const neverNullable = <T>(data: T) => isNullable(data) ? throwError(new Error()) : of(data as NonNullable<T>);
