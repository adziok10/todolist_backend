import { Injectable } from '@nestjs/common';

import { UserStoreService } from 'src/shared/services/user-store.service';
import { RedisClientService } from 'src/shared/services/redis-client.service';

@Injectable()
export class TodoCacheService {
    constructor(
        private readonly userStore: UserStoreService,
        private readonly redisClientService: RedisClientService,
    ) {}

    public async removeValueFromUserCache(id: string) {
        return await this.redisClientService.client.lrem(this.userStore.user._id.toString(), -1, id.toString());
    }

    public async init() {
        // if (await this.redisClientService.client.llen(this.userStore.user._id) > 0) {
        //     const todoLists = await this.todoListRepository.find({ owner: this.userStore.user._id, limit: 100 });

        //     await this.redisClientService.client.rpush(this.userStore.user._id, ...todoLists.map(({ _id }) => _id));
        // }
    }

    public async pushValueToUserCache(id: string) {
        return await this.redisClientService.client.rpush(this.userStore.user._id.toString(), id.toString());
    }

    public async isExists(id: string): Promise<boolean> {
        const list: string[] = await this.redisClientService.client.lrange(this.userStore.user._id.toString(), 0, -1) || [];
        console.log(list.includes(id))
        return list.includes(id);
    }

}
