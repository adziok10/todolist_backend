import { Injectable, Scope } from '@nestjs/common';

import { IAuthUser } from '../interfaces/auth-user.interface';

@Injectable({ scope: Scope.REQUEST })
export class UserStoreService {
    private $user: IAuthUser;

    set user(u: IAuthUser) {
        this.$user = u;
    }

    get user(): IAuthUser {
        return this.$user;
    }
}
