import { Injectable, Scope } from '@nestjs/common';
import { RedisService } from 'nestjs-redis';
import * as Redis from 'ioredis';

@Injectable({ scope: Scope.DEFAULT })
export class RedisClientService {
    private $client: Redis.Redis;

    constructor(
        private readonly redisService: RedisService,
    ) {
        this.$client = this.redisService.getClient();
    }

    get client() {
        return this.$client;
    }

}
