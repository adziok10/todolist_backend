import {
    Injectable,
    ExecutionContext,
    NestInterceptor,
    CallHandler,
} from '@nestjs/common';
import { Observable } from 'rxjs';

@Injectable()
export class CorsInterceptor implements NestInterceptor {

    intercept(
        context: ExecutionContext,
        next: CallHandler<any>,
    ): Observable<any> {
        const response = context.switchToHttp().getResponse();

        response.header('Access-Control-Request-Methods', 'POST, GET, OPTIONS, PUT, DELETE');
        response.header('Access-Control-Allow-Origin', '*');
        response.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');

        return next.handle();
    }

}
