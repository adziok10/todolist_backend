FROM node:13.7

WORKDIR /app

RUN npm install -g rimraf

ADD package.json /app/

COPY . /app/

RUN yarn install

EXPOSE 4000

RUN yarn build

CMD [ "yarn", "start:prod" ]
