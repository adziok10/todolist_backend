const fs = require('fs');

const config = {
    name: 'Todo',
    nameToLowerCase: 'todo',
    schema: 'Todo',
    schemaLowerCase: 'todo'
};

const startWithLowerCase = (t) => t[0].toLowerCase()+t.slice(1,Infinity)

const moduleWithModel = `import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { ${config.name}Controller } from './${config.nameToLowerCase}.controller';
import { ${config.name}Service } from './${config.nameToLowerCase}.service';
import { ${config.schema}Schema } from './model/${config.schemaLowerCase}.model';
import { ${config.schema}Repository } from './model/${config.schemaLowerCase}.repository';

@Module({
    imports: [
        MongooseModule.forFeature([{
            name: '${config.schema}',
            schema: ${config.schema}Schema,
        }]),
    ],
    controllers: [${config.name}Controller],
    providers: [${config.name}Service, ${config.schema}Repository],
    exports: [${config.name}Service],
})
export class ${config.name}Module {}
`;

const defaultSchema = `import { Schema, Document } from 'mongoose';

export const ${config.schema}Schema = new Schema({
}, {
    timestamps: true,
});

export class I${config.schema} {
    // tslint:disable-next-line: variable-name
    _id: string | any;

    createdAt: Date;
    updatedAt: Date;
}

export interface M${config.schema} extends Document, I${config.schema} {}
`

const defaultRepository = `import { Injectable, Logger } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';

import { M${config.schema} } from './${config.schemaLowerCase}.model';

const logger = new Logger('${config.schema}Repository');

@Injectable()
export class ${config.schema}Repository {
    constructor(@InjectModel('${config.schema}') private readonly ${startWithLowerCase(config.schema)}Model: Model<M${config.schema}>) { }
}
`;

const defaultService = `import { Injectable } from '@nestjs/common';

import { ${config.schema}Repository } from './model/${config.schemaLowerCase}.repository';

@Injectable()
export class ${config.name}Service {
    constructor(private ${startWithLowerCase(config.schema)}Repository: ${config.schema}Repository) {}

}
`;

const defaultController = `import { Controller, Get } from '@nestjs/common';

import { ${config.name}Service } from './${config.nameToLowerCase}.service';

@Controller('/${config.name.toLowerCase()}')
export class ${config.name}Controller {
    constructor(private readonly ${config.name.toLowerCase()}Service: ${config.name}Service) {}

    @Get()
    public ping() {
        return 'pong';
    }

}
`;


if(!fs.existsSync(`./src/modules/${config.nameToLowerCase}`)){
    fs.mkdirSync(`./src/modules/${config.nameToLowerCase}`, 0766);
    fs.appendFileSync(`./src/modules/${config.nameToLowerCase}/${config.nameToLowerCase}.module.ts`, moduleWithModel);
    fs.mkdirSync(`./src/modules/${config.nameToLowerCase}/model`, 0766);
    fs.mkdirSync(`./src/modules/${config.nameToLowerCase}/dto`, 0766);
    fs.appendFileSync(`./src/modules/${config.nameToLowerCase}/model/${config.schemaLowerCase}.model.ts`, defaultSchema);
    fs.appendFileSync(`./src/modules/${config.nameToLowerCase}/model/${config.schemaLowerCase}.repository.ts`, defaultRepository);
    fs.appendFileSync(`./src/modules/${config.nameToLowerCase}/${config.nameToLowerCase}.controller.ts`, defaultController);
    fs.appendFileSync(`./src/modules/${config.nameToLowerCase}/${config.nameToLowerCase}.service.ts`, defaultService);
}
